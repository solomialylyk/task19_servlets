package com.solomia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/users/*")
public class UserServlet extends HttpServlet {
    private static Logger logger1 = LogManager.getLogger(UserServlet.class);
    private static Map<Integer, Pizza> pizzaMap = new HashMap<>();

    public void init() throws ServletException {

        logger1.info("Servlet " + this.getServletName() + " has started");
        Pizza pizza = new Pizza("Havaii", new City("Lviv"));
        Pizza pizza1 = new Pizza("Cheese", new City("Lviv"));
        Pizza pizza2 = new Pizza("Bavarski", new City("Lviv"));
        pizzaMap.put(pizza.getId(), pizza);
        pizzaMap.put(pizza1.getId(), pizza1);
        pizzaMap.put(pizza2.getId(), pizza2);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info("Into doGet()");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h2> Pizza List</h2>");
        for (Pizza p : pizzaMap.values()) {
            out.println("<p>"+ p+ "</p>");
        }
        out.println("<form action='users' method='POST'>\n"
                + "Pizza Name: <input type='text' name='pizza_name'> \n"
                + "Address: <input type='text' name='pizza_address'> \n"
                + "<button type='submit'>Send to server</button>\n" +
                "   </form>");
        out.println("<form>\n"
                + "<p><b> DELETE ELEMENT </b></p>\n"
                +"<p> Pizza id: <input type = 'text' name = 'pizza_id'>\n"
                +"<input type= 'button' onclick ='remove(this.form.pizza_id.value)' name ='ok' value = 'Delete element'"
                + "</p>\n"
                + "</form>");
        out.println("<script type = 'text/javascript'>\n"
                + "function remove(id) { fetch('users/'+ id, {method:'DELETE'}); }\n "
                + "</script>");
        // Echo client's request information
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info("Into doPost() " + req.getParameter("pizza_name"));
        String newPizzaName = req.getParameter("pizza_name");
        String newPizzaAddress = req.getParameter("pizza_address");
        logger1.info("add " + newPizzaName);
        //City city = new City("Kyiv");
        Pizza newPizza = new Pizza(newPizzaName, new City(newPizzaAddress));
        pizzaMap.put(newPizza.getId(), newPizza);
        doGet(req, resp);
    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info("doDelete");
        String id = req.getRequestURI();
        logger1.info(" URL = " + id);
        id = id.replace("/servlets-1.0/users/","");
        logger1.info("id = " + id);
        pizzaMap.remove(Integer.parseInt(id));
       // doGet(req, resp);
    }

    public void destroy() {
        logger1.info("Servlet " + this.getServletName() + " has stopped");
    }
}
