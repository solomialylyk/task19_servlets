package com.solomia;

public class Pizza {
    private static int unique = 1;
    private int id;
    private String name;
    private City city;

    public Pizza(String name, City city) {
        this.name = name;
        this.city = city;
        id = unique;
        unique++;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address=" + city +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
