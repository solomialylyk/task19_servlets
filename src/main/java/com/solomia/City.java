package com.solomia;

public class City {
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "City{" +
                "address='" + address + '\'' +
                '}';
    }
}
